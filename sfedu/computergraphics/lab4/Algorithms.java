package sfedu.computergraphics.lab4;

import javafx.geometry.Point2D;
import javafx.scene.layout.Pane;
import javafx.scene.shape.Line;

public class Algorithms {

    public static void clipWithCyrusBeck(LineBuilder line, ConvexPolygonBuilder viewport) {
        /* +1.0 for CW, -1.0 for CCW */
        double indexOrder = viewport.getSignum();
        double tBottomMax = 0.0;
        double tTopMin = 1.0;
        int edges = viewport.getEdgesCount();
        Point2D segmentOrigin = line.getBeginPoint();
        Point2D segmentVector = line.getVector();
        for (int i = 0; i < edges; i++) {
            Point2D edgeOrigin = viewport.getNthPoint(i);
            Point2D edgeVector = viewport.getNthVector(i);
            Point2D normalVector = new Point2D(-indexOrder * edgeVector.getY(), indexOrder * edgeVector.getX()).normalize();
            Point2D edgeToSegmentVector = segmentOrigin.subtract(edgeOrigin);
            double angle = getAngleBetweenVectors(normalVector, segmentVector);
            double segmentOffset = -findEdgeAndSegmentIntersection(normalVector, edgeToSegmentVector, segmentVector);
            if (Math.abs(angle) < 0.5 * Math.PI) {
                tBottomMax = Math.max(tBottomMax, segmentOffset);
            }
            if (Math.abs(angle) > 0.5 * Math.PI) {
                tTopMin = Math.min(tTopMin, segmentOffset);
            }
        }
        if (tBottomMax < tTopMin) {
            line.setVisibleSegment(line.getPointAt(tBottomMax), line.getPointAt(tTopMin));
        }
    }

    private static double findEdgeAndSegmentIntersection(Point2D normalVector, Point2D edgeToSegmentVector, Point2D segmentVector) {
        return normalVector.dotProduct(edgeToSegmentVector) / normalVector.dotProduct(segmentVector);
    }

    public static double getAngleBetweenVectors(Point2D a, Point2D b) {
        return Math.atan2(
                a.getX() * b.getY() - a.getY() * b.getX(),
                a.getX() * b.getX() + a.getY() * b.getY()
        );
    }
}
