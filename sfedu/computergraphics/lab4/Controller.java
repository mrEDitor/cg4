package sfedu.computergraphics.lab4;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller implements Initializable {

    public static final String BRIEFINGS[] = {
            "Добавьте точки отсекающего выпуклого многоугольника",
            "Добавьте точки отрисовываемых отрезков"
    };
    public static final int STATUS_VIEWPORT = 0;
    public static final int STATUS_DRAWING = 1;

    @FXML
    public Label briefing;
    @FXML
    public Button toPreviousStepButton;
    @FXML
    public Button toNextStepButton;
    @FXML
    public Pane mainPane;

    private ConvexPolygonBuilder viewport;
    private ArrayList<LineBuilder> drawing;
    private int status;

    public Controller() {
        viewport = new ConvexPolygonBuilder();
        drawing = new ArrayList<>();
        status = STATUS_VIEWPORT;
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        briefing.setText(BRIEFINGS[STATUS_VIEWPORT]);
        mainPane.getChildren().addAll(viewport.getActors());
        mainPane.sceneProperty().addListener(
                (obsScene, oldScene, newScene) -> newScene.getAccelerators().put(
                        new KeyCodeCombination(KeyCode.Z, KeyCombination.CONTROL_DOWN), this::cancel
                )
        );
    }

    private void cancel() {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.cancel();
                break;
        }
    }

    public void toPreviousStep(ActionEvent event) {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.clear();
                break;
            case STATUS_DRAWING:
                viewport.unbuild();
                drawing.forEach(lineBuilder -> mainPane.getChildren().removeAll(lineBuilder.getActors()));
                drawing.clear();
                toNextStepButton.setDisable(false);
                status = STATUS_VIEWPORT;
                briefing.setText(BRIEFINGS[STATUS_VIEWPORT]);
                break;
        }
    }

    public void previewNextStep(MouseEvent mouseEvent) {
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.preview();
                break;
        }
    }

    public void toNextStep(ActionEvent event) {
        switch (status) {
            case STATUS_VIEWPORT:
                if (viewport.isConvex(false)) {
                    viewport.build();
                    toNextStepButton.setDisable(true);
                    status = STATUS_DRAWING;
                    briefing.setText(BRIEFINGS[STATUS_DRAWING]);
                }
                break;
        }
    }

    public void onMouseClicked(MouseEvent event) {
        final double x = event.getX();
        final double y = event.getY();
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.fixLastPoint(x, y);
                break;
            case STATUS_DRAWING:
                final int lines = drawing.size();
                if (lines == 0 || drawing.get(lines - 1).isComplete()) {
                    LineBuilder lineBuilder = new LineBuilder(x, y);
                    mainPane.getChildren().addAll(lineBuilder.getActors());
                    drawing.add(lineBuilder);
                } else {
                    LineBuilder lineBuilder = drawing.get(lines - 1);
                    lineBuilder.fixLastPoint(x, y);
                    Algorithms.clipWithCyrusBeck(lineBuilder, viewport);
                }
                break;
        }
    }

    public void onMouseMoved(MouseEvent event) {
        final double x = event.getX();
        final double y = event.getY();
        switch (status) {
            case STATUS_VIEWPORT:
                viewport.moveLastPoint(x, y);
                break;
            case STATUS_DRAWING:
                final int lines = drawing.size();
                if (lines > 0 && !drawing.get(lines - 1).isComplete()) {
                    drawing.get(lines - 1).moveLastPoint(x, y);
                }
                break;
        }
    }
}
