package sfedu.computergraphics.lab4;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Polyline;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Stack;

/**
 * Convex polygon with unvalidated last point, designed to trace a mouse pointer
 */
public class ConvexPolygonBuilder {

    private Polyline shape;
    private Line closingLine;
    private Stack<Double> angleSignum;

    public ConvexPolygonBuilder() {
        angleSignum = new Stack<>();
        angleSignum.push(0.0);
        shape = new Polyline(0, 0);
        shape.setStroke(Color.BLACK);
        shape.setStrokeWidth(2.0);
        closingLine = new Line();
        closingLine.setVisible(false);
        closingLine.setStroke(Color.BLACK);
        closingLine.getStrokeDashArray().addAll(5.0, 10.0);
        closingLine.setStrokeWidth(2.0);
    }

    public Collection<Shape> getActors() {
        ArrayList<Shape> actors = new ArrayList<>();
        actors.add(shape);
        actors.add(closingLine);
        return actors;
    }

    public void clear() {
        angleSignum.clear();
        angleSignum.push(0.0);
        shape.getPoints().setAll(0.0, 0.0);
        closingLine.setVisible(false);
    }

    public void fixLastPoint(double x, double y) {
        moveLastPoint(x, y);
        if (isConvex(true)) {
            if (shape.getPoints().size() == 2) {
                closingLine.setEndX(x);
                closingLine.setEndY(y);
                angleSignum.push(0.0);
            } else if (shape.getPoints().size() == 4) {
                closingLine.setVisible(true);
                angleSignum.push(0.0);
            } else if (angleSignum.peek() == 0.0) {
                Point2D previousVector = getNthVector(getPointsCount() - 3);
                Point2D activeVector = getNthVector(getPointsCount() - 2);
                angleSignum.push(Math.signum(Algorithms.getAngleBetweenVectors(previousVector, activeVector)));
            } else {
                angleSignum.push(angleSignum.peek());
            }
            shape.getPoints().addAll(x, y);
        }
    }

    public void moveLastPoint(double x, double y) {
        final int coordinates = shape.getPoints().size();
        shape.getPoints().set(coordinates - 2, x);
        shape.getPoints().set(coordinates - 1, y);
        final Color paint = isConvex(true) ? Color.BLACK : Color.RED;
        shape.setStroke(paint);
        closingLine.setStartX(x);
        closingLine.setStartY(y);
        closingLine.setStroke(paint);
    }

    public void cancel() {
        if (angleSignum.size() > 1) {
            angleSignum.pop();
            int pointsCount = getPointsCount();
            shape.getPoints().remove(2 * pointsCount - 2, 2 * pointsCount);
            Point2D lastPoint = getNthPoint(pointsCount - 2);
            moveLastPoint(lastPoint.getX(), lastPoint.getY());
            closingLine.setVisible(shape.getPoints().size() != 2);
        }
    }

    public void preview() {
        Point2D firstPoint = getNthPoint(0);
        moveLastPoint(firstPoint.getX(), firstPoint.getY());
        final Color color = isConvex(false) ? Color.GREEN : Color.RED;
        shape.setStroke(color);
        closingLine.setStroke(color);
    }

    public void build() {
        Point2D firstPoint = getNthPoint(0);
        moveLastPoint(firstPoint.getX(), firstPoint.getY());
        shape.setStroke(Color.BLUE);
        closingLine.setVisible(false);
    }

    public void unbuild() {
        closingLine.setVisible(true);
    }

    public boolean isConvex(boolean allowIncomplete) {
        final int pointsCount = getPointsCount();
        if (pointsCount == 1) {
            return allowIncomplete;
        }
        Point2D activeVector = getNthVector(pointsCount - 2);
        if (activeVector.distance(0.0, 0.0) == 0.0) {
            return false;
        }
        if (pointsCount <= 3) {
            return allowIncomplete;
        }
        Point2D firstVector = getNthVector(0);
        Point2D previousVector = getNthVector(pointsCount - 3);
        Point2D closingVector = getNthVector(pointsCount - 1);
        double targetAngleSignum = this.angleSignum.peek();
        if (targetAngleSignum == 0.0) {
            return true;
        }
        double firstAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(closingVector, firstVector));
        double activeAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(previousVector, activeVector));
        double closingAngleSignum = Math.signum(Algorithms.getAngleBetweenVectors(activeVector, closingVector));
        return (firstAngleSignum == targetAngleSignum || firstAngleSignum == 0.0)
                && (activeAngleSignum == targetAngleSignum || activeAngleSignum == 0.0)
                && (closingAngleSignum == targetAngleSignum || closingAngleSignum == 0.0);
    }

    public Point2D getNthPoint(int n) {
        int upperBound = shape.getPoints().size();
        Double x = shape.getPoints().get((2 * n) % upperBound);
        Double y = shape.getPoints().get((2 * n + 1) % upperBound);
        return new Point2D(x, y);
    }

    public int getPointsCount() {
        return shape.getPoints().size() / 2;
    }

    public int getEdgesCount() {
        return shape.getPoints().size() / 2 - 1;
    }

    public Point2D getNthVector(int n) {
        Point2D firstPoint = getNthPoint(n);
        Point2D secondPoint = getNthPoint(n + 1);
        return secondPoint.subtract(firstPoint);
    }

    /**
     * Get signum of inner angles between edges
     *
     * @return +1.0 for CW-drawn polygon and -1.0 for CCW-drawn
     */
    public double getSignum() {
        return angleSignum.peek();
    }
}
