package sfedu.computergraphics.lab4;

import javafx.geometry.Point2D;
import javafx.scene.paint.Color;
import javafx.scene.shape.Line;
import javafx.scene.shape.Shape;

import java.util.ArrayList;
import java.util.Collection;

public class LineBuilder {

    private Line originalLine, visibleLine;
    private boolean complete;

    public LineBuilder(double x, double y) {
        originalLine = new Line(x, y, x, y);
        originalLine.setStroke(Color.BLACK);
        originalLine.getStrokeDashArray().setAll(2.5, 5.0);
        originalLine.setStrokeWidth(2.0);
        visibleLine = new Line(x, y, x, y);
        visibleLine.setStroke(Color.BLACK);
        visibleLine.setStrokeWidth(2.0);
        complete = false;
    }

    public Collection<Shape> getActors() {
        ArrayList<Shape> actors = new ArrayList<>();
        actors.add(originalLine);
        actors.add(visibleLine);
        return actors;
    }

    public boolean isComplete() {
        return complete;
    }

    public void fixLastPoint(double x, double y) {
        moveLastPoint(x, y);
        originalLine.setStroke(Color.RED);
        complete = true;
    }

    public void moveLastPoint(double x, double y) {
        originalLine.setEndX(x);
        originalLine.setEndY(y);
    }

    public void setVisibleSegment(Point2D start, Point2D end) {
        visibleLine.setStartX(start.getX());
        visibleLine.setStartY(start.getY());
        visibleLine.setEndX(end.getX());
        visibleLine.setEndY(end.getY());
    }

    public Point2D getPointAt(double t) {
        Point2D start = new Point2D(originalLine.getStartX(), originalLine.getStartY());
        Point2D end = new Point2D(originalLine.getEndX(), originalLine.getEndY());
        return start.add(end.subtract(start).multiply(t));
    }

    public Point2D getVector() {
        return new Point2D(
                originalLine.getEndX() - originalLine.getStartX(),
                originalLine.getEndY() - originalLine.getStartY()
        );
    }

    public Point2D getBeginPoint() {
        return new Point2D(originalLine.getStartX(), originalLine.getStartY());
    }
}
